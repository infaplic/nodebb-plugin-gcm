"use strict";
/* globals define, socket, app */

define('forum/gcm/settings', ['vendor/jquery/serializeObject/jquery.ba-serializeobject.min'], function() {
	var Settings = {};

	Settings.init = function() {
		socket.emit('plugins.gcm.settings.load', function(err, settings) {
			var	defaults = {
					'gcm:enabled': 1
				};

			for(var key in defaults) {
				if (defaults.hasOwnProperty(key)) {
					if (settings[key] === null) {
						settings[key] = defaults[key];
					}
				}
			}

			// Load settings
			$('.gcm-settings #enabled').prop('checked', parseInt(settings['gcm:enabled'], 10) === 1);
			$('.gcm-settings #target').val(settings['gcm:target']);
		});

		$('#save').on('click', function() {
			var settings = $('.gcm-settings').serializeObject();
			settings['gcm:enabled'] = settings['gcm:enabled'] === 'on' ? 1 : 0;

			socket.emit('plugins.gcm.settings.save', settings, function(err) {
				if (!err) {
					app.alertSuccess('[[user:profile_update_success]]');
				} else {
					app.alertError(err.message || '[[error:invalid-data]]');
				}
			});
		});

		$('#test').on('click', function() {
			socket.emit('plugins.gcm.test', function(err) {
				if (!err) { app.alertSuccess('Test notification sent'); }
				else { app.alertError(err.message); }
			});
		});

		$('#disassociate').on('click', Settings.disassociate);
	};

	return Settings;
});