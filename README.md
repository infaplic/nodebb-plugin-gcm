# GCM Notifications

Allows NodeBB to interface with the GCM service in order to provide push notifications to user mobile phones.

## Installation

    npm install nodebb-plugin-gcm

## Configuration

1. Install and activate this plugin.
2. Enter Google API Key, and save
1. Reload NodeBB.

## Screenshots

![NodeBB Notifications GCM](screenshots/plugin_activation.png)
![Admin panel setup](screenshots/plugin_settings.png)
![User Settings](screenshots/user_settings.png)
