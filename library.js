"use strict";

var db = module.parent.require('./database'),
	meta = module.parent.require('./meta'),
	user = module.parent.require('./user'),
	posts = module.parent.require('./posts'),
	topics = module.parent.require('./topics'),
	translator = module.parent.require('../public/src/modules/translator'),
	SocketPlugins = module.parent.require('./socket.io/plugins'),

	winston = module.parent.require('winston'),
	nconf = module.parent.require('nconf'),
	async = module.parent.require('async'),
	request = module.parent.require('request'),
	querystring = require('querystring'),
	striptags = require('striptags'),
	escapeHtml = require('escape-html'),
	unescapeHtml = require('unescape-html'),
	path = require('path'),
	cache = require('lru-cache'),
	url = require('url'),

	constants = Object.freeze({
		push_url: 'https://fcm.googleapis.com/fcm/send'
	}),

	Gcm = {};

Gcm.init = function(data, callback) {
	var pluginMiddleware = require('./middleware')(data.middleware),
		pluginControllers = require('./controllers');

	// Admin setup routes
	data.router.get('/admin/plugins/gcm', data.middleware.admin.buildHeader, pluginControllers.renderACP);
	data.router.get('/api/admin/plugins/gcm', pluginControllers.renderACP);

	data.router.get('/gcm/settings', data.middleware.buildHeader, pluginMiddleware.isLoggedIn, pluginControllers.renderSettings);
	data.router.get('/api/gcm/settings', pluginMiddleware.isLoggedIn, pluginControllers.renderSettings);

	// Config set-up
	db.getObject('settings:gcm', function(err, config) {
		if (!err && config) {
			Gcm.config = config;
		} else {
			winston.info('[plugins/gcm] Please complete setup at `/admin/gcm`');
		}
	});

	// WebSocket listeners
	SocketPlugins.gcm = {
		settings: {
			save: Gcm.settings.save,
			load: Gcm.settings.load
		},
		test: Gcm.test
	};

	callback();
};

Gcm.completeSetup = function(req, res, next) {
	winston.info('[plugins/gcm] setup complete.');
};


Gcm.test = function(socket, data, callback) {
	winston.info('[plugins/gcm] test.');
	if (socket.uid) {
		Gcm.push({
			notification: {
				title: 'Test Notification',
				pid: 'Test Notification',
				path: nconf.get('relative_path') + '/',
				bodyShort: 'Test Notification',
				bodyLong: 'If you have received this, then Gcm is now working!'
			},
			uids: [socket.uid]
		});
		callback();
	} else {
		callback(new Error('[[error:not-logged-in]]'));
	}
};

Gcm.push = function(data) {
	var notifObj = data.notification;
	var uids = data.uids;

	if (!Array.isArray(uids) || !uids.length || !notifObj) {
		return;
	}

	var settingsKeys = uids.map(function(uid) {
		return 'user:' + uid + ':settings';
	});
	
	winston.info('[plugins/gcm] config. ' + JSON.stringify(Gcm.config));
	winston.info('[plugins/gcm] push. ' + JSON.stringify(settingsKeys));
	
	
	async.parallel({
		tokens: async.apply(db.getObjectFields, 'gcm:tokens', uids),
		settings: async.apply(db.getObjectsFields, settingsKeys, ['gcm:enabled', 'gcm:target', 'topicPostSort', 'language'])
	}, function(err, results) {
		if (err) {
			return winston.error(err.stack);
		}

		winston.info('[plugins/gcm] push. results. ' + JSON.stringify(results));
		
		if (results.hasOwnProperty('tokens')) {
			uids.forEach(function(uid, index) {
				winston.info('[plugins/gcm] push. results.settings. ' + JSON.stringify(results.settings[index]));
				if (!results.settings[index]) {
					return;
				}
				winston.info('[plugins/gcm] push. results.settings. gcm:enabled. ' + results.settings[index]['gcm:enabled']);
				if (results.settings[index]['gcm:enabled'] === null || parseInt(results.settings[index]['gcm:enabled'], 10) === 1) {
					pushToUid(uid, notifObj, results.settings[index]);
				}
			});
		}
	});
};

function pushToUid(uid, notifObj, settings) {
	
	winston.info('[plugins/gcm] pushToUid. uid. ' + uid);
	winston.info('[plugins/gcm] pushToUid. notifObj. ' + JSON.stringify(notifObj));
	winston.info('[plugins/gcm] pushToUid. settings. ' + JSON.stringify(settings));
	
	if (notifObj.hasOwnProperty('path')) {
		var urlObj = url.parse(notifObj.path, false, true);
		if (!urlObj.host && !urlObj.hostname) {
			// This is a relative path
			if (notifObj.path.startsWith('/')) {
				notifObj.path = notifObj.path.slice(1);
			}
			notifObj.path = url.resolve(nconf.get('url') + '/', notifObj.path);
		}
	}

	async.waterfall([
		function(next) {
			var language = settings.language || meta.config.defaultLang || 'en-GB',
				topicPostSort = settings.topicPostSort || meta.config.topicPostSort || 'oldest_to_newest';

			notifObj.bodyLong = escapeHtml(striptags(unescapeHtml(notifObj.bodyLong || '')));
			async.parallel({
				title: async.apply(topics.getTopicFieldByPid, 'title', notifObj.pid ),
				text: function(next) {
					translator.translate(notifObj.bodyShort, language, function(translated) {
						next(undefined, striptags(translated));
			 		});
				},
				postIndex: function(next) {
					posts.getPostField(notifObj.pid, 'tid', function(err, tid) {
						if (err) {
							return next(err);
						}
						posts.getPidIndex(notifObj.pid, tid, topicPostSort, next);
					});
				},
				topicSlug: async.apply(topics.getTopicFieldByPid, 'slug', notifObj.pid)
			}, next);
		},
		function(data, next) {

			winston.verbose('[plugins/gcm] data. ' + JSON.stringify(data));
			var	payload = {
					to: '/topics/user.' + uid,
					notification: {
						title: data.title != 'null' && data.title.length > 0 ? data.title : data.text,
						body: data.title ? data.text : notifObj.bodyLong,
						icon: Gcm.config.icon,
						url: notifObj.path || nconf.get('url') + '/topic/' + data.topicSlug + '/' + data.postIndex,
					}
				};
			var options = { method: 'POST',
					  url: constants.push_url,
					  headers: { 
						  'cache-control': 'no-cache',
						  authorization: 'key='+ Gcm.config.key,
						  'content-type': 'application/json' 
					  },
					  body: payload,
					  json: true };
			
			winston.verbose('[plugins/gcm] options. ' + JSON.stringify(options));
			winston.verbose('[plugins/gcm] Sending push notification to uid ' + uid);
			request(options, function (error, response, body) {
				if (error) {
					winston.error('[plugins/gcm (uid: ' + uid  + ')] err. ' + error);
					winston.error('[plugins/gcm (uid: ' + uid  + ')] err.text. ' + JSON.stringify(error));
				} else if (body.length) {
					winston.info('[plugins/gcm (uid: ' + uid  + ')] result. ' + body);
					try {
						result = JSON.parse(body);
						winston.error('[plugins/gcm (uid: ' + uid  + ')] result. ' + result);
						if (result.hasOwnProperty('error') && result.error.type === 'invalid_user') {
							winston.info('[plugins/gcm] uid ' + uid + ' has disassociated, removing token.');
							Gcm.disassociate({
								uid: uid
							});
						} else if (result.hasOwnProperty('error')) {
							winston.error('[plugins/gcm (uid: ' + uid  + ')] ' + result.error.message + ' (' + result.error.type + ')');
						}
					} catch (e) {
						winston.error('[plugins/gcm (uid: ' + uid  + ')] ' + e);
					}
				}
			});
		}
	]);
}

Gcm.addMenuItem = function(custom_header, callback) {
	winston.info('[plugins/gcm] entering addMenuItem.');
	custom_header.plugins.push({
		"route": '/plugins/gcm',
		"icon": 'fa-mobile',
		"name": 'GCM'
	});

	callback(null, custom_header);
};

Gcm.addProfileItem = function(data, callback) {
	winston.info('[plugins/gcm] entering addProfileItem.');
	if (Gcm.config && Gcm.config.key) {
		data.links.push({
			id: 'gcm',
			route: '../../gcm/settings',
			icon: 'fa-mobile',
			name: 'GCM',
			visibility: {
				self: true,
				other: false,
				moderator: false,
				globalMod: false,
				admin: true,
			}
		});
	}

	callback(null, data);
};



/* Settings */
Gcm.settings = {};

Gcm.settings.save = function(socket, data, callback) {
	if (socket.hasOwnProperty('uid') && socket.uid > 0) {
		db.setObject('user:' + socket.uid + ':settings', data, callback);
	} else {
		callback(new Error('not-logged-in'));
	}
};

Gcm.settings.load = function(socket, data, callback) {
	if (socket.hasOwnProperty('uid') && socket.uid > 0) {
		db.getObjectFields('user:' + socket.uid + ':settings', ['gcm:enabled', 'gcm:target'], callback);
	} else {
		callback(new Error('not-logged-in'));
	}
};

module.exports = Gcm;
