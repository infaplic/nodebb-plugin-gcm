<div class="row gcm">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="fa fa-mobile"></i> GCM Notifications</div>
			<div class="panel-body">
				<p class="lead">
					Allows NodeBB to interface with the GCM service in order to provide push notifications to user mobile phones.
				</p>
				<ol>
					<li>Install and activate this plugin.</li>
					<li>Enter the Server key into the configuration block below, and save.</li>
					<li>Reload NodeBB.</li>
				</ol>
				<div class="row">
					<div class="col-sm-12 well">
						<form class="form gcm-settings">
							<div class="form-group">
								<label for="key">Server key</label>
								<input type="text" class="form-control" id="key" name="key" />
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 well">
						<form class="form gcm-settings">
							<div class="form-group">
								<label for="key">Public icon URL</label>
								<input type="text" class="form-control" id="icon" name="icon" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">GCM Control Panel</div>
			<div class="panel-body">
				<button class="btn btn-primary" id="save">Save Settings</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	require(['settings'], function(Settings) {
		Settings.load('gcm', $('.gcm-settings'));

		$('#save').on('click', function() {
			Settings.save('gcm', $('.gcm-settings'), function() {
				app.alert({
					type: 'success',
					alert_id: 'gcm-saved',
					title: 'Reload Required',
					message: 'Please reload your NodeBB to complete configuration of the GCM plugin',
					clickfn: function() {
						socket.emit('admin.reload');
					}
				})
			});
		});
	});
</script>