<div class="row gcm settings">
	<div class="col-xs-12">
		<div class="panel panel-success">
			<div class="panel-body">
				<h1>GCM Notification Settings</h1>
				<p class="lead">
					Customise your GCM integration here.
				</p>

				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2 class="panel-title">General Settings</h2>
							</div>
							<div class="panel-body">
								<form role="form" class="form gcm-settings">
									<div class="checkbox">
										<label for="enabled">
											<input id="enabled" name="gcm:enabled" type="checkbox" /> Enable GCM Notifications
										</label>
									</div>
									<button type="button" class="btn btn-primary" id="save">Save Settings</button>
									<button type="button" class="btn btn-link" id="test">Send Test Notification</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>