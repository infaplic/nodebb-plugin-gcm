<div class="row gcm authSuccess">
	<div class="col-xs-12">
		<div class="panel panel-success">
			<div class="panel-body">
				<h1>Your account is now associated with GCM!</h1>
				<p>
					Congratulations! You should now be able to receive notifications to all associated GCM devices.
				</p>
				<p>
					<div class="btn-group">
						<a href="{config.relative_path}/gcm/settings" class="btn btn-primary">Manage GCM Settings</a>
						<a href="{config.relative_path}/" class="btn btn-default">Back to Index</a>
					</div>
				</p>
			</div>
		</div>
	</div>
</div>