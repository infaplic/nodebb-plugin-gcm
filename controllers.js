var Gcm = require('./library'),
	meta = module.parent.parent.require('./meta'),
	nconf = module.parent.parent.require('nconf'),
	

	Controllers = {};

Controllers.renderACP = function(req, res) {
	res.render('admin/plugins/gcm', {
		base_url: nconf.get('url').replace(/\/+$/, '')
	});
};

Controllers.renderAuthSuccess = function(req, res) {
	res.render('gcm/assocSuccess');
};

Controllers.renderSettings = function(req, res) {
	res.render('gcm/settings');
};

module.exports = Controllers;